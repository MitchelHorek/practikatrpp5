from main import main


def test():
    assert float(main("Fahrenheit", 32, "Celsius")) == 0
    assert float(main("Kelvin", 273.15, "Celsius")) == 0
    assert float(main("Celsius", 32, "Kelvin")) == 305.15
    assert float(main("Celsius", 45, "Fahrenheit")) == 113
    assert float(main("Fahrenheit", 23, "Kelvin")) == 268.15
    assert float(main("Kelvin", 308.15, "Fahrenheit")) == 95


